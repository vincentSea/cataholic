//index.js
//获取应用实例
const app = getApp()
const img = '../../images/cat-backyard-bg.jpg';
Page({
  data: {
    width: 0,
    height: 0,
    motto: 'Hello World Cat-Aholic',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    wx.getSystemInfo({
      success: res => {
        console.log(res)
        this.setData({
          width: res.windowWidth,
          height: res.windowHeight
        });

        const ctx = wx.createCanvasContext('catmap')
        var ratio = res.windowWidth / res.windowHeight;
        console.log(ratio)

        ctx.drawImage(
          img,
          0,
          0,
          res.windowWidth,
          res.windowHeight
        )

        ctx.drawImage(
          '../../images/cat-shop.png',
          -40 * res.windowWidth / 750,
          305 * res.windowHeight / 1334,
          300 * res.windowWidth / 750,
          200 * res.windowWidth / 750
        )
        // ctx.setFillStyle('red')
        // ctx.fillRect(50 * res.windowWidth / 750, 330 * res.windowHeight / 1334, 100 * res.windowWidth / 750, 100 * res.windowWidth / 750)
        ctx.draw()
      },
    })
    
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        console.log(app.globalData.userInfo)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  touchstart: function(e) {
    console.log(e);
  }
})
