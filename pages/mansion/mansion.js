// pages/mansion/mansion.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  getPreviewImage: function(res) {
    wx.previewImage({
      urls: ['http://d3n5f32jnf9jcb.cloudfront.net/thumbnail/2018/1/29/23545aeb9ce44d64a9d79ee7ced58ea2_672x378.jpg','http://d3n5f32jnf9jcb.cloudfront.net/thumbnail/2017/12/19/3206f3ed2bdf469da41b00005a1b2aa4_222x164.jpg'],
    })
  }
})