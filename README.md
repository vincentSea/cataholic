## 吸猫项目

基于微信小程序开发

### cataholic
> 在英语中，词根"-aholic" 表示“对……成瘾，嗜好……的”。

>（Liking sth very much and unable to stop doing or using it.）

```
购物狂：a shopaholic

嗜食巧克力的恶人：a chocaholic

吸猫的人：a cataholic
```

### 目录
* 目录结构会跟着业务增加
```
├── app.wxss             # 入口样式
├── common               # 公用布局
├── images               # 图片资源
├── jsconfig.json       
├── pages                # 小程序页面
│   ├── addalbum         # 发写真
│   ├── album            # 相册
│   ├── backyard         # 猫咪-后院
│   ├── card             # 猫咪-名片
│   ├── club             # 猫咪-会所
│   ├── draft            # 选秀馆
│   ├── help             # 帮助
│   ├── home             # 首页
│   ├── index            # 初始页
│   ├── join             # 猫咪-入驻
│   ├── logs             # 日志
│   ├── mansion          # 猫公馆
│   ├── palace           # 猫咪-皇宫
│   ├── shop             # 猫咪-商店
│   └── star             # 猫星榜
├── project.config.json
├── utils                # 公用工具
└── weui.wxss            # weui 样式组件 （后续是否考虑去掉）
```

### 开发准备
* [微信小程序API](https://mp.weixin.qq.com/debug/wxadoc/dev/api)
* [微信小程序组件](https://mp.weixin.qq.com/debug/wxadoc/dev/component)
* [微信开发者工具](https://mp.weixin.qq.com/debug/wxadoc/dev/devtools/download.html)

### 接口文档

[文档地址](http://confluence.lab.tclclouds.com/pages/viewpage.action?pageId=21830515)
```
http://confluence.lab.tclclouds.com/pages/viewpage.action?pageId=21830515
```

### [尺寸单位](https://mp.weixin.qq.com/debug/wxadoc/dev/framework/view/wxss.html)
> rpx（responsive pixel）: 可以根据屏幕宽度进行自适应。规定屏幕宽为750rpx。如在 iPhone6 上，屏幕宽度为375px，共有750个物理像素，则750rpx = 375px = 750物理像素，1rpx = 0.5px = 1物理像素。


|   设备          | rpx换算px (屏幕宽度/750)  |  px换算rpx (750/屏幕宽度)   |
| -------------  | ----------------------: | :-----------------------: |
| iPhone5        | 1rpx = 0.42px           |   1px = 2.34rpx           |
| iPhone6        | 1rpx = 0.5px            |   1px = 2rpx              |
| iPhone6 Plus   | 1rpx = 0.552px          |   1px = 1.81rpx           |


### 版本管理

* 使用git做为版本管理

|   分支      |  说明       |   
| ---------  | :--------:  | 
| master     | 线上稳定版本  |     
| dev        | 开发版本      |

